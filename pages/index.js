import { Input } from 'antd'
import styles from '../styles/Home.module.css'
import { AutoComplete } from 'antd'
import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import Head from 'next/head'
import TempCards from '../components/TempCards'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer'
import axios from 'axios'
import Comments from '../components/Comments'
import BlogCard from '../components/BlogCard'
import { faFacebook, faFacebookF, faGoogle, faInstagram, faLinkedin, faPinterest, faTwitter, faYoutube, faQuora } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Skeleton } from 'antd'


const selectData = [
  {
    value: 'node'
  },
  {
    value: 'java'
  },
  {
    value: 'python'
  },
  {
    value: 'javascript'
  },
  {
    value: 'jdk'
  },
  {
    value: 'pip'
  },
  {
    value: 'ubuntu'
  },
  {
    value: 'golang'
  }
]



const mockVal = (str) => {
  
	let _data = selectData.filter(e => (e.value.includes(str)))

	return _data

}

const Home = ({ dockerfileList, blog }) => {

  const [value, setValue] = useState('')
  const [options, setOptions] = useState([])
  const [ske, setSke] = useState(true)

  const router = useRouter()

  const onSearch = (searchText) => {
    setOptions(!searchText ? [] : mockVal(searchText))
  }

  const onSelect = (data) => {
    router.push(`/platform/${data}`)
  }

  const onChange = (data) => {
    setValue(data);
  }

  return (
    <div>

      <Head>
          <title>Dockerfile</title>
      </Head>
      <Navbar />

      <div className={styles.iconbar}>
        <a href="https://www.youtube.com/channel/UC5L60bs5Z4Y5rqjuPjodl8Q" target="_blank" rel="noreferrer" className={styles.youtube}><FontAwesomeIcon icon={faYoutube} /></a>
        <a href="#" className={styles.instagram}><FontAwesomeIcon icon={faInstagram} /></a>
        <a href="#" className={styles.pinterest}><FontAwesomeIcon icon={faPinterest} /></a>
        <a href="#" className={styles.facebook}><FontAwesomeIcon icon={faFacebook} /></a>
        <a href="https://matrixxcode.quora.com/" target="_blank" rel="noreferrer" className={styles.quora}><FontAwesomeIcon icon={faQuora} /></a>
      </div>

      <div className={styles.emptydiv}></div>


      <div className={styles.indexcontainer}>
        <h1 className={styles.head}>Dockerfile</h1>
      </div>

      <div className={styles.container}>

        <AutoComplete
          value={value}
          options={options}
          onSelect={onSelect}
          onSearch={onSearch}
          onChange={onChange}
          className={styles.search}
        >

          <Input.Search size="large" placeholder="Search the platform with editor" enterButton />

        </AutoComplete>

      </div>

      <div className={styles.templatecontainer}>
		    <h1>Use Template</h1>
      </div>

		<div>
			<TempCards dockerfileList={dockerfileList} ske={ske} setSke={setSke} />
		</div>

    <div className={styles.blog}>

        {
          blog.map(list => (
                <BlogCard key={1} list={list} />
            ))
        }

    </div>

    <div className={styles.comment}>
      <Comments />
    </div>

    <Footer />

    </div>
  );
}


// data from server
export async function getStaticProps() {

  const res = await axios.get(`https://dockerfile-server.herokuapp.com/api/v1/template/list?limit=4&skip=1`)
  const blogRes = await axios.get(`https://dockerfile-server.herokuapp.com/api/v1/blog/list`)

  const dockerfileList = res.data.templates
  const blog = blogRes.data

  return {
      props: {
          dockerfileList,
          blog
      }
  }

}

export default Home