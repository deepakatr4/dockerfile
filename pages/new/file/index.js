import { ChakraProvider } from "@chakra-ui/react";
import NewDockerFile from "../../../components/NewDockerFile";


const NewFile = () => {
    return (
        <div>

            <ChakraProvider>
                <NewDockerFile />
            </ChakraProvider>

        </div>
    );
}

export default NewFile