import { useRouter } from "next/router";
import Editor from "../../../components/Editor";
import File from "../../../components/File";
import styles from '../../../styles/Platform.module.css'
import Head from 'next/head'
import { useState } from 'react';


const Platform = () => {

    const router = useRouter()

    console.log(router)

    const [baseImage, setBaseImage] = useState(router.query.name)
    const [version, setVersion] = useState('latest')
    const [imageType, setImageType] = useState('apline')
    const [workDir, setWorkDir] = useState('app')
    const [port, setPort] = useState('3000')
    const [command, setCommand] = useState('node index')

    return (
        <div className={styles.container}>

            <Head>
                <title>Dockerfile</title>
            </Head>

            <File 
                baseImage={baseImage}
                workDir={workDir}
                port={port}
                command={command}
                version={version}
                imageType={imageType}
            />

            <Editor
                baseImage={baseImage}
                setBaseImage={setBaseImage}
                version={version}
                setVersion={setVersion}
                imageType={imageType}
                setImageType={setImageType}
                workDir={workDir}
                setWorkDir={setWorkDir}
                port={port}
                setPort={setPort}
                command={command}
                setCommand={setCommand}
            />

        </div>
    );
}


export default Platform