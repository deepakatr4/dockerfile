import { Pagination, Spin } from 'antd';
import axios from 'axios';
import { useState } from 'react';
import { useEffect } from 'react';
import Cards from '../../components/Card';
import Navbar from '../../components/Navbar';
import { useRouter } from 'next/router';
import styles from '../../styles/Template.module.css';
import Footer from '../../components/Footer'
import Head from 'next/head'

const Templates = () => {

    const [list, setList] = useState([])
    const [spinning, setSpinning] = useState(true)
    const [templatesCount, setTemplatesCount] = useState(0)

    const router = useRouter()

    useEffect(() => {

        const limit = 4

        let skip = parseInt(router?.query?.page ? router?.query?.page : 1)

        if(skip === 1) skip = 0
        else skip = 4 * (skip - 1)

        const fetchData = async () => {

            const templates = await axios
                        .get(`https://dockerfile-server.herokuapp.com/api/v1/template/list?limit=${limit}&skip=${skip}`)
    
            const dockerfileList = templates.data.templates
            const templateCount = templates.data.totalCount
    
            if(Array.isArray(dockerfileList)) {
                setList(dockerfileList)
                setTemplatesCount(templateCount)
                setSpinning(false)
            }

        }

        fetchData()

    }, [router.query.page])

    const handlePagination = (page, pageSize) => {
        setSpinning(true)
        router.query.page = page
        router.push({ pathname: router.pathname, query: { page } })
    }

    return (
        <div>

            <Head>
                <title>Templates</title>
            </Head>

            <Navbar />

            <Pagination defaultCurrent={templatesCount} total={templatesCount} pageSize={4} onChange={handlePagination} className={styles.pagination} />

            <Spin size="large" spinning={spinning}>

                <div className={styles.templatecontainer}>

                    <div className={styles.tempcards}>
                        { 
                            list.map(file => (<Cards className={styles.tempcard} key={file._id} file={file} />)) 
                        }
                    </div>
                    
                </div>

            </Spin>

            <Footer />


        </div>
    );
}


// export async function getServerSideProps (context) {

//     const limit = 4

//     let skip = parseInt(context?.query?.page ? context?.query?.page : 0)

//     if(skip === 1) skip = 0
//     else skip = 4 * (skip - 1)

//     const templates = await axios
//                 .get(`https://dockerfile-server.herokuapp.com/api/v1/template/list?limit=${limit}&skip=${skip}`)

//     const dockerfileList = templates.data.templates
//     const templateCount = templates.data.totalCount

//     return {
//         props: {
//             dockerfileList,
//             templateCount
//         }
//     }

// }


export default Templates