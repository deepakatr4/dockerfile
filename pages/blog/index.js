import Navbar from '../../components/Navbar'
import styles from '../../styles/Blog.module.css'
import Image from 'next/image'
import BlogCard from '../../components/BlogCard';
import Footer from '../../components/Footer'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import axios from 'axios';
import { Spin } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faInstagram, faLinkedin, faPinterest, faQuora, faYoutube } from '@fortawesome/free-brands-svg-icons';

const Blog = () => {


    const [blog, setBlog] = useState([])
    const [spin, setSpin] = useState(true)

    useEffect(() => {

        const fetchData = async () => {
            const res = await axios.get(`https://dockerfile-server.herokuapp.com/api/v1/blog/list`)
            if(res.status === 200) {
                setBlog(res.data)
                setSpin(false)
            }
        }

        fetchData()

    })


    return (
        <div>

            <Head>
                <title>Blog</title>
            </Head>

            <Navbar />


            <h4>Blog</h4>

            <Spin size='large' spinning={spin}>

                <div className={styles.blogtcontainer}>
                    <div className={styles.bloggrid}>
                        { 
                            blog.map(list => (
                                <BlogCard key={1} list={list} />
                            ))
                        }
                    </div>
                </div>

                <div className={styles.iconbar}>
                    <a href="https://www.youtube.com/channel/UC5L60bs5Z4Y5rqjuPjodl8Q" target="_blank" rel="noreferrer" className={styles.youtube}><FontAwesomeIcon icon={faYoutube} /></a>
                    <a href="#" className={styles.instagram}><FontAwesomeIcon icon={faInstagram} /></a>
                    <a href="#" className={styles.pinterest}><FontAwesomeIcon icon={faPinterest} /></a>
                    <a href="#" className={styles.facebook}><FontAwesomeIcon icon={faFacebook} /></a>
                    <a href="https://matrixxcode.quora.com/" target="_blank" rel="noreferrer" className={styles.quora}><FontAwesomeIcon icon={faQuora} /></a>
                </div>

            </Spin>


            <Footer />

        </div>
    );
}

export default Blog