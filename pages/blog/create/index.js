import CreateBlogCom from '../../../components/CreateBlogCom';
import Navbar from '../../../components/Navbar'

const CreateBlog = () => {
    return (
        <div>

            <Navbar />

            <h4>Create Blog</h4>

            <CreateBlogCom />


        </div>
    );
}

export default CreateBlog