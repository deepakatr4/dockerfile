import dynamic from "next/dynamic";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faInstagram, faLinkedin, faPinterest, faQuora, faYoutube } from '@fortawesome/free-brands-svg-icons';
import styles from '../../../styles/Blog.module.css'
import Navbar from '../../../components/Navbar'


const BlogRenderer = dynamic(() => import('../../../components/BlogRenderer'), {
    ssr: false
})

const ViewBlog = ({ _id }) => {
    return (
        <div>

            <Navbar />

            <div className={styles.iconbar}>
                <a href="https://www.youtube.com/channel/UC5L60bs5Z4Y5rqjuPjodl8Q" target="_blank" rel="noreferrer" className={styles.youtube}><FontAwesomeIcon icon={faYoutube} /></a>
                <a href="#" className={styles.instagram}><FontAwesomeIcon icon={faInstagram} /></a>
                <a href="#" className={styles.pinterest}><FontAwesomeIcon icon={faPinterest} /></a>
                <a href="#" className={styles.facebook}><FontAwesomeIcon icon={faFacebook} /></a>
                <a href="https://matrixxcode.quora.com/" target="_blank" rel="noreferrer" className={styles.quora}><FontAwesomeIcon icon={faQuora} /></a>
            </div>

            <BlogRenderer _id={_id} />

        </div>
    );
}


export async function getServerSideProps({ params }) {

    return {
        props: {
            _id: params._id
        }
    }

}

export default ViewBlog
