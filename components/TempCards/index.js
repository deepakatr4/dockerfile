import { useState } from 'react'
import Cards from "../Card";
import styles from '../../styles/Home.module.css'
import { useEffect } from 'react';
import { Skeleton } from 'antd'

const TempCards = ({ dockerfileList, ske, setSke }) => {

    const [template, setTemplate] = useState([])

    useEffect(() => {
        if(Array.isArray(dockerfileList)) {
            setTemplate(dockerfileList)
            setSke(false)
        }
    }, [dockerfileList, ske, setSke])


    return (
        <div className={styles.tempcards}>

            { template.map(file => (
                <div key={file._id}>
                    <Skeleton loading={ske} active={true}  />
                    <Cards className={styles.tempcard} key={file._id} file={file} />
                </div>
            )) }
            
        </div>
    );
}



export default TempCards