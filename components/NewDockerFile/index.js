import Navbar from "../Navbar";
import styles from '../../styles/NewFile.module.css'
import { FormControl, FormLabel, Input, Textarea, useToast } from "@chakra-ui/react";
import { Button, Upload } from "antd";
import { useState } from "react";
import axios from "axios";
import Head from 'next/head'
import Footer from '../Footer'
import dynamic from 'next/dynamic'

const CodeEditor = dynamic(() => import('../RichEditor/CodeEditor'), {
  ssr: false
})


const NewDockerFile = () => {

    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [image, setImage] = useState('')
    const [dockerfile, setDockerfile] = useState('')
    

    const toast = useToast()

    const fileChangedHandler = event => {
      if(event.target.files[0]?.size > 50000) {
          toast({
          title: "Image Size",
          description: "Please reduce the Image size below 50Kb",
          status: "warning",
          duration: 9000,
          isClosable: true,
        })
        setImage('')
      }
      else setImage(event.target.files[0])
    }

    const onSubmit = async () => {

      const formData = new FormData()
      formData.append(
        'image',
        image,
        image.name
      )
      formData.append('title', title)
      formData.append('description', description)
      formData.append('text', dockerfile)

      const res = await axios.post(`https://dockerfile-server.herokuapp.com/api/v1/template/add`, formData, {
        onUploadProgress: progressEvent => {
          console.log(progressEvent.loaded / progressEvent.total)
        }
      })

      if(res.status === 200) {
        toast({
          title: "Dockerfile Created",
          description: "Check the Template page",
          status: "success",
          duration: 9000,
          isClosable: true,
        })
      }

    }

    return (
        <div>

            <Head>
                <title>Create Dockerfile</title>
            </Head>

            <Navbar />

            <div className={styles.container}>
                
                <div className={styles.form}>
                    
                    <FormControl className={styles.formcontrol}>

                        <FormLabel>Title</FormLabel>
                        <Input type="text" value={title} onChange={e => setTitle(e.target.value)} />

                    </FormControl>

                    <FormControl className={styles.formcontrol}>

                        <FormLabel>Description</FormLabel>
                        <Input type="text" value={description} onChange={e => setDescription(e.target.value)} />

                    </FormControl>

                    <FormControl className={styles.formcontrol}>

                        <FormLabel>Image</FormLabel>
                        <input type="file" onChange={fileChangedHandler} accept="image/png" />

                    </FormControl>

                    <FormControl className={styles.formcontrol}>

                        <FormLabel>Dockerfile</FormLabel>
                        {/* <Textarea placeholder="Here is a sample placeholder" size="lg" value={dockerfile} onChange={e => setDockerfile(e.target.value)} /> */}
                        <CodeEditor />

                    </FormControl>

                    <div className={styles.createdockerbtn}>
                        <Button onClick={onSubmit}>Create Dockerfile</Button>
                    </div>

                </div>

            </div>

            <Footer />

        </div>
    );
}

export default NewDockerFile