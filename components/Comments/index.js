import { Input } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import axios from 'axios';
import { useState } from 'react';
import styles from '../../styles/Comments.module.css'
import { message } from 'antd';



const Comments = () => {

    const [comment, setComment] = useState('')
    const [email, setEmail] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')

    const handleSubmit = async () => {
        let data = {
            comment,
            email,
            phonenumber: phoneNumber
        }

        setComment('')
        setEmail('')
        setPhoneNumber('')

        let res = await axios.post(`https://dockerfile-server.herokuapp.com/api/v1/comment/add`, data)

        if(res.status === 200) message.success('Thank you for your valuable comment')

    }

    return (
        <div className={styles.container}>
            
            <div className={styles.commentcontainer}>

                <div className={styles.commentpro}>
                    <h2>Make a Comment</h2>

                    <TextArea rows={5} className={styles.textarea} placeholder="Write your comments here..."
                        value={comment} onChange={e => setComment(e.target.value)}
                    />

                    <Input placeholder="Email" className={styles.email} 
                        value={email} onChange={e => setEmail(e.target.value)}
                    />

                    <Input placeholder="Phone Number" className={styles.phoneNumber} 
                        value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)}
                    />

                    <button className={styles.button} onClick={handleSubmit}>Make a Comment <span></span></button>
                </div>

            </div>

        </div>
    );
}

export default Comments