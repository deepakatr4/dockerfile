import styles from '../../styles/Navbar.module.css'
import Link from 'next/link'

const Navbar = () => {
    return (
        <div className={styles.navcontainer}>
            <div className={styles.header}>
                <input className={styles.menubtn} type="checkbox" id="menu-btn" />
                <label className={styles.menuicon} htmlFor="menu-btn"><span className={styles.navicon}></span></label>
                <ul className={styles.menu}>
                    <li><Link href="/">Home</Link></li>
                    <li><Link href="/new/file">Create Dockerfile</Link></li>
                    <li><Link href="/template?page=1">Templates</Link></li>
                    <li><Link href="/blog">Blog</Link></li>
                </ul>
            </div>
        </div>
    );
}

export default Navbar