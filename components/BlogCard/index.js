import styles from '../../styles/Blog.module.css'
import { useRouter } from 'next/router'
import moment from 'moment'

const BlogCard = ({ list }) => {

    const router = useRouter()

    const handleBlogClick = (_id) => {
        router.push(`/blog/view/${_id}`, undefined, { shallow: true })
    }

    return (
        <div onClick={e => handleBlogClick(list._id)}>

            <div className={styles.card}>
                <div className="card-header">
                </div>
                <div className={styles.cardbody}>
                <span className={styles.tag}>{list.tag}</span>
                <h4>
                    {list.title}
                </h4>
                <p>
                    {list.description}
                </p>
                <div className={styles.user}>
                    <div className={styles.userinfo}>
                        <h5>Created at</h5>
                        <small>{moment(list.createdDate).fromNow()}</small>
                    </div>
                </div>
                </div>
            </div>

        </div>
    );
}

export default BlogCard