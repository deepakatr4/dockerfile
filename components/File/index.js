import styles from '../../styles/Platform.module.css'
import { Button } from 'antd'
import { saveAs } from 'file-saver'

const File = (props) => {

    const genDockerfile = () => {

        let data = document.getElementById('fileinfo')
        let newData = data.innerHTML.replaceAll('<p>', '')

        let splitedData = newData.split('</p>')

        let toCreate = ''

        for(let i = 0; i < splitedData.length; i ++) {
            toCreate = toCreate + splitedData[i] + '\n'
        }

        var blob = new Blob([toCreate],
                { type: "text/text;charset=utf-8" })
        saveAs(blob, "Dockerfile")

    }

    return (
        <div className={styles.filecontainer}>

            <Button type="primary" danger className={styles.genbtn} onClick={genDockerfile}>Generate Dockerfile</Button>

            <div className={styles.fileinfo} id="fileinfo">

                <p>FROM {props.baseImage}:{props.version}-{props.imageType}</p>
                <p>WORKDIR {props.workDir}</p>
                <p>COPY package.json /{props.workDir}</p>
                <p>RUN npm install</p>
                <p>COPY . /{props.workDir}</p>
                <p>EXPOSE {props.port}</p>
                <p>CMD {props.command}</p>

            </div>

        </div>
    );
}


export default File