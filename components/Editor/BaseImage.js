import { Menu, Dropdown, Button } from 'antd';


const BaseImage = ({ baseImage, setBaseImage }) => {

    const menu = (
      <Menu>
        <Menu.Item onClick={() => setBaseImage('node')}>
            node
        </Menu.Item>
        <Menu.Item onClick={() => setBaseImage('java')}>
            java
        </Menu.Item>
        <Menu.Item onClick={() => setBaseImage('go')}>
            go
        </Menu.Item>
      </Menu>
    );

    return (
        <div>

            
            <Dropdown overlay={menu} placement="bottomCenter" arrow trigger={['click']}>
                <Button type='primary' size='large' style={{ width: '10rem' }} > {baseImage} </Button>
            </Dropdown>
            

        </div>
    );
}


export default BaseImage