import { Input } from 'antd'


const WorkDir = ({ workDir, setWorkDir }) => {
    return (
        <div>
            
            <Input placeholder="Work Dir" value={workDir} onChange={e => setWorkDir(e.target.value)} />                      

        </div>
    );
}


export default WorkDir