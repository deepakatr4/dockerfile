import { Menu, Dropdown, Button } from 'antd';


const ImageType = ({ imageType, setImageType }) => {

    const menu = (
      <Menu>
        <Menu.Item onClick={() => setImageType('alpine')}>
            alpine
        </Menu.Item>
        <Menu.Item onClick={() => setImageType('ubuntu')}>
            ubuntu
        </Menu.Item>
        <Menu.Item onClick={() => setImageType('centos')}>
            centos
        </Menu.Item>
      </Menu>
    );

    return (
        <div>

            
            <Dropdown overlay={menu} placement="bottomCenter" arrow trigger={['click']}>
                <Button type='primary' size='large' style={{ width: '10rem' }} > {imageType} </Button>
            </Dropdown>
            

        </div>
    );
}


export default ImageType