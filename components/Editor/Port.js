import { Input } from 'antd'


const Port = ({ port, setPort }) => {
    return (
        <div>

            <Input placeholder="Port" value={port} onChange={e => setPort(e.target.value)} />                        

        </div>
    );
}


export default Port