import styles from '../../styles/Platform.module.css'
import BaseImage from './BaseImage';
import ImageType from './ImageType';
import Version from './Version';
import WorkDir from './WorkDir';
import Command from './Command';
import Port from './Port';


const Editor = ({ baseImage, setBaseImage, version, setVersion, imageType, setImageType, workDir, setWorkDir, port, setPort, command, setCommand }) => {

    
    return (
        <div className={styles.editorcontainer}>

            <div className={styles.editorsubcontainer}>

                <table className={styles.table}>
                    <tbody>
                        <tr>
                            <td>
                                <p>Base Image</p>
                            </td>
                            <td>
                                <BaseImage baseImage={baseImage} setBaseImage={setBaseImage} />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <p>Version</p>
                            </td>
                            <td>
                                <Version version={version} setVersion={setVersion} />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <p>Image Type</p>
                            </td>
                            <td>
                                <ImageType imageType={imageType} setImageType={setImageType} />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <p>Working Dir</p>
                            </td>
                            <td>
                                <WorkDir workDir={workDir} setWorkDir={setWorkDir} />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <p>Port</p>
                            </td>
                            <td>
                                <Port port={port} setPort={setPort} />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <p>Command</p>
                            </td>
                            <td>
                                <Command command={command} setCommand={setCommand} />
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>
    );
}


export default Editor