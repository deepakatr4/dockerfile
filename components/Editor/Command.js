import { Input } from 'antd'

const Command = ({ command, setCommand }) => {
    return (
        <div>
            
            <Input placeholder="Command" value={command} onChange={e => setCommand(e.target.value)} />

        </div>
    );
}


export default Command