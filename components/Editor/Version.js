import { Menu, Dropdown, Button } from 'antd';


const Version = ({ version, setVersion }) => {
  const menu = (
    <Menu>
      <Menu.Item onClick={() => setVersion('latest')}>
          latest
      </Menu.Item>
      <Menu.Item onClick={() => setVersion('14')}>
          14
      </Menu.Item>
      <Menu.Item onClick={() => setVersion('10')}>
          10
      </Menu.Item>
    </Menu>
  );

  return (
      <div>

          
          <Dropdown overlay={menu} placement="bottomCenter" arrow trigger={['click']}>
              <Button type='primary' size='large' style={{ width: '10rem' }} > {version} </Button>
          </Dropdown>
          

      </div>
  );
}


export default Version