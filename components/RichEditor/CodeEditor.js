import EditorJS from '@editorjs/editorjs'
const CodeTool = require('@editorjs/code')
import { useState, useEffect } from 'react';

const CodeEditor = () => {

    const [data, setData] = useState('')

    const [editor, setEditor] = useState()

    useEffect(() => {

        setEditor(new EditorJS({
            holder : 'editor',
            tools: {
                code: {
                  class: CodeTool,
                  inlineToolbar : true
                }
            },
            placeholder: 'Let`s write an awesome Code!',
            data: data
        }))

    }, [])

    
    const handleSave = async () => {
        const savedData = await editor.save();
        console.log(savedData)
        setData(savedData)  
    }

    return (
        <div>
            <span id="editor"></span>

            <button onClick={handleSave}>Save</button>
        </div>
    );
}

export default CodeEditor;