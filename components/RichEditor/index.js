import EditorJS from '@editorjs/editorjs'
import Embed from "@editorjs/embed";
import Table from "@editorjs/table";
import List from "@editorjs/list";
import Warning from "@editorjs/warning";
import Code from "@editorjs/code";
import LinkTool from "@editorjs/link";
import Image from "@editorjs/image";
import Raw from "@editorjs/raw";
import Header from "@editorjs/header";
import Quote from "@editorjs/quote";
import Marker from "@editorjs/marker";
import CheckList from "@editorjs/checklist";
import Delimiter from "@editorjs/delimiter";
import InlineCode from "@editorjs/inline-code";
import SimpleImage from "@editorjs/simple-image";
import { useState, useEffect } from 'react';
import styles from '../../styles/RichEditor.module.css'
import axios from 'axios';

const RichEditor = () => {

    const [data, setData] = useState('')
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [tag, setTag] = useState('')

    const [editor, setEditor] = useState()

    useEffect(() => {

        setEditor(new EditorJS({
            holder : 'editor',
            tools: {
                header: {
                  class: Header,
                  inlineToolbar : true
                },
                table: {
                  class: Table,
                  inlineToolbar : true
                },
                list: {
                  class: List,
                  inlineToolbar : true
                },
                warning: {
                  class: Warning,
                  inlineToolbar : true
                },
                embed: {
                  class: Embed,
                  inlineToolbar : true
                },
                code: {
                  class: Code,
                  inlineToolbar : true
                },
                linkTool: {
                  class: LinkTool,
                  inlineToolbar : true
                },
                image: {
                  class: Image,
                  inlineToolbar : true,
                  config: {
                    endpoints: {
                      byFile: 'https://dockerfile-server.herokuapp.com/api/v1/blog/image/upload',
                    }
                  }
                },
                raw: {
                  class: Raw,
                  inlineToolbar : true
                },
                quote: {
                  class: Quote,
                  inlineToolbar : true
                },
                marker: {
                    class: Marker,
                    inlineToolbar : true
                },
                checklist: {
                  class: CheckList,
                  inlineToolbar : true
                },
                delimiter: {
                  class: Delimiter,
                  inlineToolbar : true
                },
                inlineCode: {
                    class: InlineCode,
                    inlineToolbar : true
                },
                simpleImage: {
                  class: SimpleImage,
                  inlineToolbar : true
                },
            },
            placeholder: 'Let`s write an awesome Blog!',
            data: data
        }))

    }, [])

    

    const handleSave = async () => {
        const savedData = await editor.save();
        setData(savedData)

        const _data = {
          title, description, tag, body: savedData
        }

        const res = await axios.post(`https://dockerfile-server.herokuapp.com/api/v1/blog/create`, _data)

        if(res.status === 200) alert('created')

    }

    return (
        <div>

            <button onClick={handleSave} className={styles.btn}>Create the Blog</button>

             <form>

                <label>Title</label>
                <input className={styles.input} type="text" placeholder="Title goes here..." value={title} onChange={e => setTitle(e.target.value)} />

                <label>Description</label>
                <input className={styles.input} type="text" placeholder="Description goes here..." value={description} onChange={e => setDescription(e.target.value)} />

                <label>Tag</label>
                <input className={styles.input} type="text" placeholder="Tag goes here..." value={tag} onChange={e => setTag(e.target.value)} />

            </form>

            <span id="editor"></span>

        </div>
    );
}

export default RichEditor;