import styles from '../../styles/Footer.module.css'
import { InstagramOutlined } from '@ant-design/icons/lib/icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram, faPinterest, faQuora } from '@fortawesome/free-brands-svg-icons';

const Footer = () => {

    // var scrollToTopBtn = document.getElementById("scrollToTopBtn")
    // var rootElement = document.documentElement

    // function scrollToTop() {
    //     rootElement.scrollTo({
    //         top: 0,
    //         behavior: "smooth"
    //     })
    // }
      
    // scrollToTopBtn.addEventListener("click", scrollToTop)

    return (
        <div className={styles.footer}>

            <div className={styles.footergrid}>

                <div className={styles.footergridel}>
                    <h4>Services</h4>
                    <ul>
                        <li>
                            <InstagramOutlined />
                            <a href="https://www.instagram.com/gd_solutions_tech/" target="_blank" rel="noreferrer">Instagram</a>
                        </li>
                        <li>
                            <InstagramOutlined />
                            <a href="https://www.instagram.com/gd_solutions_tech/" target="_blank" rel="noreferrer">Instagram</a>
                        </li>
                        <li>
                            <InstagramOutlined />
                            <a href="https://www.instagram.com/gd_solutions_tech/" target="_blank" rel="noreferrer">Instagram</a>
                        </li>
                    </ul>
                </div>

                <div className={styles.footergridel}>
                    <h4>Contact</h4>
                    <ul>
                        <li>
                            <InstagramOutlined />
                            <a href="https://www.instagram.com/gd_solutions_tech/" target="_blank" rel="noreferrer">Instagram</a>
                        </li>
                        <li>
                            <InstagramOutlined />
                            <a href="https://www.instagram.com/gd_solutions_tech/" target="_blank" rel="noreferrer">Instagram</a>
                        </li>
                        <li>
                            <InstagramOutlined />
                            <a href="https://www.instagram.com/gd_solutions_tech/" target="_blank" rel="noreferrer">Instagram</a>
                        </li>
                    </ul>
                </div>

                <div className={styles.footergridel}>
                    <h4>Follow</h4>
                    <ul>
                        <li>
                            <FontAwesomeIcon icon={faInstagram} />
                            <a href="https://www.instagram.com/gd_solutions_tech/" target="_blank" rel="noreferrer">Instagram</a>
                        </li>
                        <li>
                            <FontAwesomeIcon icon={faPinterest} />
                            <a href="https://in.pinterest.com/gdsolutionst/_saved/" target="_blank" rel="noreferrer">Pinterest</a>
                        </li>
                        <li>
                            <FontAwesomeIcon icon={faQuora} />
                            <a href="https://www.quora.com/profile/GD-Solutions-Tech" target="_blank" rel="noreferrer">Quora</a>
                        </li>
                    </ul>
                </div>

            </div>

            {/* <button id="scrollToTopBtn"></button> */}

        </div>
    );
}


export default Footer