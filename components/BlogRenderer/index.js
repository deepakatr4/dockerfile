import EditorJS from '@editorjs/editorjs'
import Embed from "@editorjs/embed";
import Table from "@editorjs/table";
import List from "@editorjs/list";
import Warning from "@editorjs/warning";
import Code from "@editorjs/code";
import LinkTool from "@editorjs/link";
import Image from "@editorjs/image";
import Raw from "@editorjs/raw";
import Header from "@editorjs/header";
import Quote from "@editorjs/quote";
import Marker from "@editorjs/marker";
import CheckList from "@editorjs/checklist";
import Delimiter from "@editorjs/delimiter";
import InlineCode from "@editorjs/inline-code";
import SimpleImage from "@editorjs/simple-image";
import { useEffect, useState } from 'react';
import axios from 'axios'
import Head from 'next/head'


const BlogRenderer = ({ _id }) => {

    // const [data, setData] = useState('')
    const [head, setHead] = useState('Blog')

    useEffect(() => {

        let data

        const fetchData = async () => {
            const res = await axios.get(`https://dockerfile-server.herokuapp.com/api/v1/blog/view/${_id}`)
            // setData(res.data.body)
            data = res.data.body
            setHead(res.data.title)
    
            var editor = new EditorJS({
                holder : 'editorjs',
                tools: {
                    header: {
                      class: Header,
                      inlineToolbar : true
                    },
                    table: {
                      class: Table,
                      inlineToolbar : true
                    },
                    list: {
                      class: List,
                      inlineToolbar : true
                    },
                    warning: {
                      class: Warning,
                      inlineToolbar : true
                    },
                    embed: {
                      class: Embed,
                      inlineToolbar : true
                    },
                    code: {
                      class: Code,
                      inlineToolbar : true
                    },
                    linkTool: {
                      class: LinkTool,
                      inlineToolbar : true
                    },
                    image: {
                      class: Image,
                      inlineToolbar : true
                    },
                    raw: {
                      class: Raw,
                      inlineToolbar : true
                    },
                    quote: {
                      class: Quote,
                      inlineToolbar : true
                    },
                    marker: {
                        class: Marker,
                        inlineToolbar : true
                    },
                    checklist: {
                      class: CheckList,
                      inlineToolbar : true
                    },
                    delimiter: {
                      class: Delimiter,
                      inlineToolbar : true
                    },
                    inlineCode: {
                        class: InlineCode,
                        inlineToolbar : true
                    },
                    simpleImage: {
                      class: SimpleImage,
                      inlineToolbar : true
                    },
                },
                readOnly: true,
                data: data
            });
        }

        fetchData()


    }, [])



    return (
        <div id="editorjs">

          <Head>
            <title>{head}</title>
          </Head>


        </div>
    );
}


export default BlogRenderer