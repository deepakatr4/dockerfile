import { Card, Avatar, Modal, Button } from 'antd';
import { EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';
import Image from 'next/image'
import styles from '../../styles/TempCard.module.css'
import { useState } from 'react'
import axios from 'axios'
import { Spin } from 'antd'

const { Meta } = Card;

const Cards = ({ file }) => {

    // const handleOnClick = () => {
    //     alert(file._id)
    // }

    const [loading, setLoading] = useState(false)
    const [visible, setVisible] = useState(false)
    const [text, setText] = useState('')
    const [spin, setSpin] = useState(false)
    
    const showModal = async () => {

        setSpin(true)

        const res = await axios.get(`https://dockerfile-server.herokuapp.com/api/v1/template/view/${file._id}`)

        setText(res.data.text)

        setSpin(false)
        setVisible(true)
    };

    const handleDownload = () => {
        setLoading(true)
        setTimeout(() => {
            setLoading(false)
            setVisible(false)
        }, 3000);
    };

    const handleCancel = () => {
        setVisible(false)
    };

    return (
        <div className={styles.tempcardcontainer}>
            <Card
                style={{ width: 300 }}
                cover={
                <Image
                    alt="example"
                    src={file.image?.buffer?.data ? 
                        `data:image/png;base64,${Buffer.from(file.image.buffer.data).toString('base64')}`
                        : '/default.png'}
                    width={400}
                    height={200}
                />
                }
                // actions={[
                //     <EditOutlined key="edit" />,
                //     <EllipsisOutlined key="ellipsis" />,
                // ]}
                onClick={showModal}
            >
                <div className={styles.meta}>
                    <Meta
                        // avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
                        title={file.title}
                        description={file.description}
                    />
                </div>
            </Card>

            <Spin spinning={spin} size="large" />
            
            <Modal
                visible={visible}
                title={file.title}
                onOk={handleDownload}
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Return
                    </Button>,
                    <Button key="submit" type="primary" loading={loading} onClick={handleDownload}>
                        Download Dockerfile
                    </Button>,
            ]}
            >
                <p>
                    {text}
                </p>
            </Modal>
            
        </div>
    );
}


export default Cards