import styles from '../../styles/CreateBlog.module.css'
import dynamic from 'next/dynamic'
import { useState } from 'react';

let RichEditor = dynamic(() => import('../RichEditor/index'), {
    ssr: false
});


const CreateBlogCom = () => {

    const [content, setContent] = useState('')

    return (
        <div className={styles.container}>

            <RichEditor />

        </div>
    );
}


export default CreateBlogCom